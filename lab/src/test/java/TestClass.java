import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;

import mountainhuts.MountainHut;
import mountainhuts.Municipality;
import mountainhuts.Region;

public class TestClass {
	
	Region region = Region.fromFile("Piemonte", "./mountain_huts.csv");
	
	@Test
	public void testAltitudeRange() {
		region.setAltitudeRanges("500-1500");
		assertEquals("500-1500", region.getAltitudeRange(750));
	}
	
	@Test
	public void testCreateOrGetMunicipality() {
		Municipality mun = region.createOrGetMunicipality("Paullo", "MI", 750);
		assertNotNull(mun);
		assertEquals(region.createOrGetMunicipality("Paullo","MI", 750), mun);
	}

	@Test
	public void testCreateOrGetMountainHut() {
		MountainHut hut = region.createOrGetMountainHut("Hut1", "Rifugio", 30, region.createOrGetMunicipality("Paullo", "MI", 750));
		assertNotNull(hut);
		assertEquals(region.createOrGetMountainHut("Hut1", "Rifugio", 30, region.createOrGetMunicipality("Paullo", "MI", 750)), hut);
	}

}
